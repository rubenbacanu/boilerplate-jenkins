#!groovy
// check https://tremend.atlassian.net/wiki/spaces/TAPM/pages/1371242600/How+to+add+a+pipeline+job+in+Jenkins for: 
    // FASTLANE_PASSWORD
    // MATCH_PASSWORD
    // TEAM_ID
    // APPLE_ID

import groovy.json.JsonSlurper

pipeline {
    agent any
    options {
        ansiColor('xterm')
    }
    
    environment {
      TERM                        = "xterm-256color"
      LANG                        = "en_US.UTF-8"
      LANGUAGE                    = "en_US.UTF-8"
      LC_ALL                      = "en_US.UTF-8"
      PATH                        = "$PATH:/usr/local/bin:$HOME/.rbenv/bin:$HOME/.rbenv/shims"
      FASTLANE_PASSWORD           = ""
      MATCH_PASSWORD              = ""

      APP_IDENTIFIER              = "yourAppIdentifier" // get this id from your project
      TEAM_ID                     = ""
      APPLE_ID                    = ""
      PROJECT_NAME                = "yourProjectName*" // project name
      APPCENTER_APP_ID            = "yourAppcenterAppId" // you can get this id from the project's url appcenter
      SLACK_CHANNEL               = "channelId" // you get that on Slack in the info section of the group
      SPACESHIP_SKIP_2FA_UPGRADE  = 1
    }

    stages { 
      stage('Sonar metrics') {
        when {
          expression {
            params.RUN_SONARQUBE == true  
          }
        }
        steps {
          script {
            try {
              withSonarQubeEnv() {
                // uncomment and keep this if your project is iOS
                    // sh '''
                    // #!/bin/bash -l
                    // eval "$(rbenv init -)"
                    // security unlock-keychain -p "${env.MATCH_PASSWORD}"

                    // fastlane sonarqube
                    // '''

                // uncomment and keep this if your project is android
                    // sh '''
                    // chmod +x gradlew
                    // ./gradlew sonarqube
                    // '''
              }
            } catch (Exception e) {
              error "Pipeline aborted due to quality gate failure: ${e}"
            }
          }
        }
      }

      stage("Quality Gate for SonarQube") {
        when {
          expression {
            params.RUN_SONARQUBE == true  
          }
        }
        steps {
          sleep(5)
          timeout(time: 2, unit: 'MINUTES') {
            /*
            Parameter indicates whether to set pipeline to UNSTABLE if Quality Gate fails
            true = set pipeline to UNSTABLE, false = don't
            */
            script {
              String qgStatus = waitForQualityGate().status
              if (qgStatus != 'OK') {
                error "Pipeline aborted due to quality gate failure"
              }
            }
          }
        }
      }

      stage('Build') {
        steps {
          // uncomment and keep this if your project is iOS
              // sh '''
              // #!/bin/bash -l
              // eval "$(rbenv init -)"
              // security unlock-keychain -p "${env.MATCH_PASSWORD}"

              // bundle install
              // bundle update
              // bundle update fastlane

              // bundle exec fastlane enterprise
              // '''

          // uncomment and keep this if your project is android
              // sh '''
              // chmod +x gradlew
              // ./gradlew clean
              // ./gradlew build
              // '''
        }
      } 
    }

    post {
      success {
        echo 'I succeeded!'
        slackSend color: 'good', message: "${env.JOB_NAME} - #${env.BUILD_NUMBER} succeeded (<https://mcsbuild.tremend.ro/view/iOS%20Projects/job/${env.JOB_NAME}/|Open>) \n<https://install.appcenter.ms/orgs/tremend/apps/${env.APPCENTER_APP_ID}/distribution_groups/public|Download> \nThis build contains: ${env.MESSAGE}", channel: "${env.SLACK_CHANNEL}"
      }

      failure {
        echo 'I failed :('
        slackSend color: 'danger', message: "${env.JOB_NAME} - #${env.BUILD_NUMBER} failed (<https://mcsbuild.tremend.ro/view/iOS%20Projects/job/${env.JOB_NAME}/|Open>)", channel: "${env.SLACK_CHANNEL}"
      }  
    }
}